//
//  ViewController.swift
//  Lection2
//
//  Created by mac on 7/11/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        let stipendia: Double = 700
        let priceGrowth: Double = 3
        
        comparison (number1: Int(arc4random()%100), number2: Int(arc4random()%100)) //Block 1. Задача 0
        kubKvadrat (number1: Int(arc4random()%10)) //Block 1. Задача 1
        otIdo (number1: Int(9), number2: Int(0)) //Block 1. Задача 2
        deliteli (chislo: Int(arc4random()%50)) //Block 1. Задача 3
        soversh() //Block 1. Задача 4
        manhattan (start: Int(1826), now: Int(2018), percent: Double(6)) //Block 2. Задача 1
        studentSurvive (stipendia: Double(stipendia), priceGrowth: Double(3)) //Block 2. Задача 2
        nakopl (stipendia: Double(stipendia), priceGrowth: Double(priceGrowth)) //Block 2. Задача 3
        
    }
    
    
    //Block 1. Задача 0. Вывести на экран наибольшее из двух чисел
    
    func comparison(number1: Int, number2: Int) {
        let firstNumber = number1
        let secondNumber = number2
        if firstNumber > secondNumber {
            print ("Первое число \(firstNumber) больше, чем второе число \(secondNumber) ")
        }
        else if secondNumber > firstNumber {
            print ("Второе число \(secondNumber) больше, чем первое число \(firstNumber) ")
        }
        else {
            print ("Первое число \(firstNumber) равно второму числу \(secondNumber) ")
        }
    }
    
    //=============================================================================================================
    
    //Block 1. Задача 1. Вывести на экран квадрат и куб числа
    
    func kubKvadrat(number1: Int) {
        let a = number1
        let b = a * a
        let c = b * a
        print (b, "это квадрат числа", number1)
        print (c, "это куб числа", number1)
    }
    
    //=============================================================================================================
    
    //Block 1. Задача 2. Вывести на экран все числа до заданного и в обратном порядке до 0
    
    func otIdo(number1: Int, number2: Int) {
        var a = number1
        var b = number2
        for i in b..<a {
            print(i)
        }
        
        while b < a {
            a = a - 1
            print (a)
        }
    }
    
}

//=============================================================================================================

//Block 1. Задача 3. Подсчитать общее количество делителей числа и вывести их

func deliteli(chislo: Int) {
    var devider = 0
    var kolichestvo = 0
    print ("Число", chislo, "имеет такие делители:")
    for devider in 1..<chislo {
        if (chislo % devider) == 0 {
            print (devider)
            kolichestvo += 1
        }
    }
    print ("Количество делителей числа \(chislo) равно \(kolichestvo) ")
}

//=============================================================================================================

//Block 1. Задача 4. Проверить, является ли заданное число совершенным и найти их

func soversh() -> Int {
    let chislo: Int = 0
    var devider: Int
    var sum: Int
    for chislo in 1...8128 {
        sum = 0
        
        for devider in 1..<chislo {
            if (chislo % devider == 0) {
                sum = sum + devider
            }
        }
        if (chislo == sum) {
            print ("число \(chislo) является совeршенным")
        }
    }
    return chislo
}

//=============================================================================================================

//Block 2. Задача 1. Остров Манхэттен

func manhattan(start: Int, now: Int, percent: Double) -> Double {
    var sum = 24.0
    let years = now - start
    var summ = 0.0
    for _ in 0..<years {
        sum += (sum * percent / 100)
        summ += sum
    }
    print ("Состояние счета в данный момент \(sum)")
    return sum
}

//=============================================================================================================

// Block 2. Задача 2. Ежемесячная стипендия студента.
func studentSurvive(stipendia: Double, priceGrowth: Double) {
    var expenses = 1000.0
    let months = 10
    let income = stipendia * Double(months)
    var summ = 0.0
    for _ in 0..<months {
        expenses = expenses + (expenses * priceGrowth / 100.0)
        summ += expenses
    }
    print ("Нужно иметь \(summ) грн., чтобы выжить")
}

//=============================================================================================================

//Block 2. Задача 3. У студента имеются накопления
func nakopl(stipendia: Double, priceGrowth: Double) -> Double {
    var bank: Double = 2400
    //        let stipendia: Double = 700
    var expenses: Double = 1000
    //        let priceGrowth: Double = 3
    var summa: Double = 0
    var months: Double = 0
    while bank > summa {
        bank += stipendia
        expenses = expenses + expenses / 100 * priceGrowth
        summa += expenses
        months += 1
    }
    print ("Студент выживет \(months) месяцев")
    return months
}



